import { useState } from "react";
import { Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../table.css";
import TableRow from "./TableRow";

export default function MainTable() {
  const stateData = [
    {
      id: 1,
      region: "Japan",
      lastInput: 123456,
      numberOfForms: 342456,
      numberOfVoters: 123456,
      update: 342456,
      code: "japan",
      getDistrict: true,
    },
    {
      id: 2,
      region: "Thailand",
      lastInput: 123456,
      numberOfForms: 342456,
      numberOfVoters: 123456,
      code: "thailand",
      update: 342456,
      getDistrict: true,
    },
    {
      id: 3,
      region: "Hong Kong",
      lastInput: 123456,
      numberOfForms: 342456,
      numberOfVoters: 123456,
      update: 342456,
      code: "hong kong",
      getDistrict: true,
    },
  ];

  const [query, setQuery] = useState<string>("");

  return (
    <>
      <div className="filter-container">
        <select
          className="dropdown-filter filter"
          defaultValue={""}
          onChange={(e) => setQuery(e.target.value)}
        >
          <option value="" disabled hidden>
            Filter
          </option>
          {stateData.map((row, rowId) => (
            <option key={rowId} value={row.code}>
              {row.region}
            </option>
          ))}
        </select>

        <input
          className="input-filter"
          type="search"
          id="input"
          placeholder="Search..."
          onChange={(e) => setQuery(e.target.value)}
        />
      </div>
      <div className="table-container">
        <Table bordered hover responsive size="md" striped>
          <thead>
            <tr className="header">
              <th className="region-th">Region</th>
              <th>Last input</th>
              <th>Number of forms</th>
              <th>Number of Voters</th>
              <th>Update</th>
            </tr>
          </thead>
          <tbody>
            {stateData
              .filter((row) => row.region.toLowerCase().includes(query))
              .map((row, index) => (
                <TableRow row={row} key={index} />
              ))}
          </tbody>
        </Table>
      </div>
    </>
  );
}
