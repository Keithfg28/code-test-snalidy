import { Container, Nav, Navbar } from "react-bootstrap";
import "../Navigation.css";
import home from "../icon/home-solid.svg";

export default function Navigation() {
  return (
    <div className="sum">
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="light"
        variant="light"
        fixed="top"
      >
        <Container>
          <Navbar.Brand href="#home">
            <img src={home} width="30px" className="logo"></img>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link>Reports</Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link href="/">Overall</Nav.Link>
              <Nav.Link eventKey={2} href="/specific">
                Specific
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
