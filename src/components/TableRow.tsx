import { useState } from "react";

export default function TableRow(props: any) {
  const [isStateExpanded, setStateIsExpanded] = useState<boolean>(false);
  const [isDistrictExpanded, setIsDistrictExpanded] = useState<boolean>(false);
  const handleExpandState = () => {
    if (isStateExpanded && isDistrictExpanded) {
      setStateIsExpanded(false);
      setIsDistrictExpanded(false);
    } else {
      setStateIsExpanded(!isStateExpanded);
    }
  };
  const handleExpandDistrict = () => {
    setIsDistrictExpanded(!isDistrictExpanded);
  };
  return (
    <>
      <tr key={props.row.index}>
        <td className="region-td" scope="row">
          <div className="region-name">
            <div className="s">S</div>
            <div>{props.row.region}</div>
          </div>
          {props.row.getDistrict ? (
            <button
              className="district-button button"
              onClick={handleExpandState}
            >
              {isStateExpanded ? "1 Districts -" : "4 Districts +"}
            </button>
          ) : null}
        </td>
        <td>{props.row.lastInput}</td>
        <td>{props.row.numberOfForms}</td>
        <td>{props.row.numberOfVoters}</td>
        <td>{props.row.update}</td>
      </tr>

      {isStateExpanded ? (
        <tr>
          <td className="region-td">
            <div className="region-name">
              <div className="s">D</div>
              <div>Central</div>
            </div>
            <button
              className="townships-button button"
              onClick={handleExpandDistrict}
            >
              {isDistrictExpanded ? "1 Townships -" : "1 Townships +"}
            </button>
          </td>
          <td>888888</td>
          <td>888888</td>
          <td>888888</td>
          <td>888888</td>
        </tr>
      ) : null}
      {isDistrictExpanded ? (
        <tr>
          <td className="region-td">
            <div className="region-name">
              <div className="s">T</div>
              <div>Middle</div>
            </div>
          </td>
          <td>555555</td>
          <td>555555</td>
          <td>555555</td>
          <td>555555</td>
        </tr>
      ) : null}
    </>
  );
}
