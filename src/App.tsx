import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import Navigation from "./components/Navigation";
import TableFilter from "./components/Specific";
import MainTable from "./components/Table";

function App() {
  return (
    <div className="App">
      <Navigation />
      <MainTable />
    </div>
  );
}

export default App;
